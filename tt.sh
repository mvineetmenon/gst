#!/bin/sh

gst-launch-0.10 -e videomixer name=mix ! ffmpegcolorspace ! xvimagesink \
    uridecodebin uri=file:///home/vineet/Desktop/sintel_trailer-480p.webm ! \
			ffmpegcolorspace ! videoscale ! \
			video/x-raw-yuv, width=540, height=360 ! \
			videobox border-alpha=1 top=-2 left=-2 top=-2 bottom=-2 !  \
			videobox border-alpha=0 top=0 left=0 ! mix. \
    videotestsrc pattern=15 ! \
			video/x-raw-yuv, width=540, height=360 ! \
			videobox border-alpha=1 top=-2 left=-2 top=-2 bottom=-2 !  \
			videobox border-alpha=0 top=0 left=-540 ! mix. \
    videotestsrc pattern=13 ! \
			video/x-raw-yuv, width=540, height=360 ! \
			videobox border-alpha=1 top=-2 left=-2 top=-2 bottom=-2 !  \
			videobox border-alpha=0 top=-360 left=0 ! mix. \
	videotestsrc pattern=0 ! \
			video/x-raw-yuv, width=540, height=360 ! \
			videobox border-alpha=1 top=-2 left=-2 top=-2 bottom=-2 !  \
			videobox border-alpha=0 top=-360 left=-540 ! mix. \
	videotestsrc pattern=3 ! \
			video/x-raw-yuv, framerate=5/1, width=1080, height=720 ! mix.
